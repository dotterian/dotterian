# Personal webpage for Alexey Ermolaev (Dotterian)

## This project is created using following packages

- create-react-app
- react-router and react-router-dom
- react-router-page-transition
- react-easy-state
- eslint and eslint-config-standard-react
