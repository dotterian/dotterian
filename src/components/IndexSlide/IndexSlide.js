import React from 'react'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import { theme } from '../../state'
import styles from './IndexSlide.scss'

class IndexSlide extends React.Component {
  componentDidMount () {
    theme.background = '#9FD6D2'
  }

  render () {
    return <div className={`${styles.slide} transition-item`}>
      <div className={styles.container}>
        <div className={styles.photo} />
        <div className={styles.textContainer}>
          <h1>Алексей Ермолаев</h1>
          <p>Web-программист</p>
          <div className={styles.social}>
            <a href='https://gitlab.com/dotterian' target='_blank' rel='noreferer nofollow'><i className='fa fa-gitlab' /></a>
            <a href='https://twitter.com/dotterian' target='_blank' rel='noreferer nofollow'><i className='fa fa-twitter' /></a>
            <a href='mailto:dotterian@dotterian.ru'><i className='fa fa-envelope' /></a>
          </div>
        </div>
      </div>
      <Link to='/portfolio' className={styles.portfolio}>Портфолио <i className='fa fa-chevron-down' /></Link>
    </div>
  }
}

export default withRouter(IndexSlide)
