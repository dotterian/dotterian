import React from 'react'
import { Link } from 'react-router-dom'
import { theme } from '../../state'
import styles from './PortfolioSlide.scss'

class PortfolioSlide extends React.Component {
  componentDidMount () {
    theme.background = '#1B676B'
  }

  render () {
    return <div className={`${styles.portfolioSlide} transition-item`}>
      <Link className={styles.toHomePage} to='/'><i className='fa fa-chevron-up' />Визитка</Link>
      <div className={styles.grid}>
        <h1>Портфолио</h1>
        <h2>Места работы</h2>
        <div>
          2008&mdash;2013  Консалтинговое Агентство «Брэнд Капитал»
          <div className={styles.thin}>
            Full-stack программист (PHP, CSS, jQuery), разработка сайтов на базе CMS Wordpress, Joomla и фреймоврка CodeIgniter
          </div>
          2013&mdash;2014  Веб-студия «Сайтекс»
          <div className={styles.thin}>
            Full-stack программист (PHP, CSS, jQuery), разработка сайтов на базе собственной CMS и доработка данной CMS
          </div>
          2014&mdash;2015  <a href='https://dns-shop.ru' target='_blank' rel='noreferer nofollow'>Сеть цифровых магазинов DNS <i className='fa fa-link' /></a>
          <div className={styles.thin}>
            Full-stack программист (PHP, SCSS, jQuery), разработка сайтов компании с использованием фреймворка Yii2
          </div>
          2016  <a href='https://kiozk.ru' target='_blank' rel='noreferer nofollow'>Zed Russia <i className='fa fa-link' /></a>
          <div className={styles.thin}>
            Full-stack программист (PHP, SCSS, jQuery), разработка проекта Kiozk.ru на фреймворке Yii2
          </div>
          2016&mdash;2017  Союз Строительный Ресурс
          <div className={styles.thin}>
            Full-stack программист (PHP, CSS, jQuery), разработка внутренних CRM-систем на фреймворке Yii2
          </div>
          2017&mdash;2018  Aussiedev Ltd.
          <div className={styles.thin}>
            Full-stack программист (PHP, CSS, jQuery/Angularjs), разработка CRM-систем на базе фреймоврков Yii1-2
          </div>
          2018&mdash;Настощее время  <a href='https://ice-pick.com' target='_blank' rel='noreferer nofollow'>Ice-Pick Lodge <i className='fa fa-link' /></a>
          <div className={styles.thin}>
            Full-stack программист (PHP, CSS, jQuery/React), поддержка и доработка сайтов компании
          </div>
        </div>
        <h2>Мои проекты</h2>
        <div>
          2018
          <div className={styles.thin}>
            Сайт и бот для Discord-чата комикса «Маревый Мир» <a target='_blank' href='http://maremir.dotterian.ru'><i className='fa fa-link' /></a> <a href='https://gitlab.com/dotterian/maremir-cinema' target='_blank' rel='noreferer nofollow'><i className='fa fa-gitlab' /></a><br />
            Криптообменник Face2Face <a href='http://face2face.dotterian.ru'><i className='fa fa-link' /></a><br />
          Данный сайт <a href='https://gitlab.com/dotterian/dotterian' target='_blank' rel='nofollow noreferer'><i className='fa fa-gitlab' /></a><br />
          </div>
        </div>
      </div>
    </div>
  }
}

export default PortfolioSlide
