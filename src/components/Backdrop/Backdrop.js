import React from 'react'
import deepmerge from 'deepmerge'
import { view } from 'react-easy-state'
import { theme } from '../../state'
import styles from './Backdrop.scss'

class Star {
  constructor (parent) {
    this.parent = parent
    const { canvas, config } = this.parent
    this.x = Math.random() * canvas.width
    this.y = Math.random() * canvas.height

    this.vx = (config.velocity - (Math.random() * 0.5))
    this.vy = (config.velocity - (Math.random() * 0.5))

    if (Math.random() < 0.5) {
      this.vx = -this.vx
    }

    if (Math.random() < 0.5) {
      this.vy = -this.vy
    }

    this.radius = config.star.width
  }

  draw = () => {
    const { x, y, radius, parent: { context } } = this
    context.beginPath()
    context.arc(x, y, radius, 0, Math.PI * 2, false)
    context.fill()
  }
}

class Constellation {
  defaults = {
    star: {
      color: 'rgba(255, 255, 255, .5)',
      width: 2
    },
    line: {
      color: 'rgba(255, 255, 255, .5)',
      width: 0.2
    },
    position: {
      x: 0,
      y: 0
    },
    width: window.innerWidth,
    height: window.innerHeight,
    velocity: 0.5,
    length: 10 + Math.random() * 10,
    stars: []
  }

  constructor (canvas, options) {
    this.canvas = canvas
    this.context = canvas.getContext('2d')
    this.config = deepmerge(this.defaults, options)
  }

  createStars = () => {
    const { config: { length }, context, canvas } = this
    context.clearRect(0, 0, canvas.width, canvas.height)
    let star

    for (let i = 0; i < length; i++) {
      this.config.stars.push(new Star(this))
      star = this.config.stars[i]
      star.draw()
    }

    this.line()
    this.animate()
  }

  animate = () => {
    const { config, canvas } = this
    for (let i = 0; i < config.length; i++) {
      let star = config.stars[i]

      if (star.y < 0 || star.y > canvas.height) {
        star.vy = -star.vy
      } else if (star.x < 0 || star.x > canvas.width) {
        star.vx = -star.vx
      }

      star.x += star.vx
      star.y += star.vy
    }
  }

  line = () => {
    const { config: { length }, config, context } = this

    for (let i = 0; i < length; i++) {
      for (let j = 0; j < length; j++) {
        const iStar = config.stars[i]
        const jStar = config.stars[j]
        const distance = Math.sqrt(Math.abs(iStar.x - jStar.x) + Math.abs(jStar.x - jStar.y))
        const opacity = Math.max(0, 30 - distance) / 30
        context.beginPath()
        context.moveTo(iStar.x, iStar.y)
        context.lineTo(jStar.x, jStar.y)
        this.context.strokeStyle = `rgba(255,255,255,${opacity})`
        context.stroke()
        context.closePath()
      }
    }
  }

  setCanvas = () => {
    this.canvas.width = this.config.width
    this.canvas.height = this.config.height
  }

  setContext = () => {
    this.context.fillStyle = this.config.star.color
    this.context.strokeStyle = this.config.line.color
    this.context.lineWidth = this.config.line.width
  }

  loop = (callback) => {
    callback()
    this.rAF = window.requestAnimationFrame(() => {
      this.loop(callback)
    })
  }

  resize = () => {
    window.cancelAnimationFrame(this.rAF)
  }

  bind = () => {
    window.addEventListener('resize', this.resize)
  }

  unbind = () => {
    window.removeEventListener('resize', this.resize)
  }

  init = () => {
    this.setCanvas()
    this.setContext()
    this.loop(this.createStars)
    this.bind()
  }
}

class Backdrop extends React.Component {
  canvas = React.createRef()
  constellation = null

  componentDidMount () {
    this.instantiate()
    window.addEventListener('resize', this.instantiate)
  }

  instantiate = () => {
    this.constellation = new Constellation(this.canvas.current, {})
    this.constellation.init()
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.instantiate)
    this.constellation.unbind()
  }

  render () {
    return <div className={styles.backdrop}>
      <canvas ref={this.canvas} style={{ backgroundColor: theme.background }} />
    </div>
  }
}

export default view(Backdrop)
