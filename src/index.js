import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import PageTransition from 'react-router-page-transition'
import IndexSlide from './components/IndexSlide/IndexSlide'
import PortfolioSlide from './components/PortfolioSlide/PortfolioSlide'
import Backdrop from './components/Backdrop/Backdrop'
import registerServiceWorker from './registerServiceWorker'
import styles from './index.scss'

class App extends React.Component {
  render () {
    return <Router>
      <div className={styles.application}>
        <Backdrop />
        <Route
          render={({ location }) => <PageTransition timeout={500}>
            <Switch location={location}>
              <Route exact path='/' component={IndexSlide} />
              <Route path='/portfolio' component={PortfolioSlide} />
              <Route component={IndexSlide} />
            </Switch>
          </PageTransition>
          } />
      </div>
    </Router>
  }
}

ReactDOM.render(<App />, document.getElementById('root'))
registerServiceWorker()
